package no.forsen.fridge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


public class FridgeContentsActivity extends Activity{
	@Override
	protected void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fridge_contents); 
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if(requestCode == Constants.ACTIVITY_DB_CONTENTS){
			if(resultCode == RESULT_OK){
				Intent i = new Intent(this, CheckInActivity.class);
				i.putExtra(Constants.ARGS_ID, data.getIntExtra(Constants.ARGS_ID, 0));
				startActivityForResult(i,Constants.ACTIVITY_CHECK_IN);
			}
		}
	}	
} // End of class FridgeContentsActivity
