package no.forsen.fridge;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment{
	public Dialog onCreateDialog(Bundle savedStateInstance){
		final Calendar c = Calendar.getInstance(); 
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		
		DatePickerDialog dpd = new DatePickerDialog(getActivity(),(OnDateSetListener) getActivity(), year, month, day);
		dpd.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
		dpd.setCanceledOnTouchOutside(false);
		dpd.setTitle(R.string.date_title);
		return dpd; 
	}

}
