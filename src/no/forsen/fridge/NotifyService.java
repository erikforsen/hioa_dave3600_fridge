package no.forsen.fridge;

import java.util.Calendar;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.text.format.DateUtils;

public class NotifyService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		int notifyId = 1; 
		Intent resultIntent = new Intent(this, FridgeContentsActivity.class);
		PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification.Builder builder = new Notification.Builder(this)
			.setContentTitle(getResources().getString(R.string.message_fridge))
			.setContentIntent(resultPendingIntent)
			.setAutoCancel(true)
			.setSmallIcon(R.drawable.ic_stat_fridge_noti_icon);
		int numFridge = 0;
		
		Calendar today = toCalendar(System.currentTimeMillis());  
		String selection = DatabaseAdapter.FRIDGE_PRODUCT_REMOVED + " is null"; 
		Cursor fridge = getContentResolver().query(DatabaseAdapter.FRIDGE_URI, null, selection, null, null);
		if(fridge == null){
			return super.onStartCommand(intent, flags, startId);
		}
		String message = "";
		while(fridge.moveToNext()){
			Calendar exp_day = toCalendar(Long.parseLong(fridge.getString(fridge.getColumnIndex(DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE))));
			long diff = exp_day.getTimeInMillis() - today.getTimeInMillis();
			int daysLeft = (int)(diff/(DateUtils.DAY_IN_MILLIS));
			if(daysLeft < 1){
				message += getResources().getString(R.string.expired_product) + fridge.getString(fridge.getColumnIndex(DatabaseAdapter.DB_PRODUCT_NAME))+"\n"; 
				numFridge++;
			}
			else if(daysLeft < Integer.parseInt(fridge.getString(fridge.getColumnIndex(DatabaseAdapter.FRIDGE_NUM_DAYS_WARNING)))){
				message += getResources().getString(R.string.expire_in) +" "+ daysLeft +": " + fridge.getString(fridge.getColumnIndex(DatabaseAdapter.DB_PRODUCT_NAME))+"\n"; 
				numFridge++;
			}
		}
		builder.setStyle(new Notification.BigTextStyle().bigText(message))
		.setNumber(numFridge);
		if(numFridge != 0)
			notificationManager.notify(notifyId,builder.build());
		
		return super.onStartCommand(intent, flags, startId);
		
	}
	
	private Calendar toCalendar(long timeinmillis){
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timeinmillis);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c; 
	}
} // End of class NotifyService
