package no.forsen.fridge;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.DialogFragment;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.DatePicker;
import android.widget.NumberPicker;

public class CheckInActivity extends Activity implements OnDateSetListener{
  
	private Cursor cur;
	private ContentValues fridge;
	private int datepickerBug = 0; 
	@Override
	protected void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		fridge = new ContentValues(); 
		Intent i = getIntent(); 
		Bundle args = i.getExtras();
		long ean =  args.getLong(Constants.ARGS_EAN);
		if(ean != 0L)
			addFromEAN(ean);  
		else
			addFromID(args.getInt(Constants.ARGS_ID)); 

	}

	private void addFromID(int id){
		
		fridge.put(DatabaseAdapter.FRIDGE_PRODUCT, id);
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(),Constants.FRAGMENT_DATEPICKER); 
	}
	private void addFromEAN(long ean){
		final long EAN = ean; 
		Uri uri = ContentUris.withAppendedId(DatabaseAdapter.DB_URI, ean); 
		String selection = DatabaseAdapter.DB_EAN + "=?"; 
		String[] selectionArgs = {String.valueOf(ean)};
		String[] projection = 
			{ 
				DatabaseAdapter.ID,
				DatabaseAdapter.DB_EAN,
				DatabaseAdapter.DB_PRODUCT_NAME
			};
		cur = getContentResolver().query(uri, projection, selection, selectionArgs, null);

		if(cur != null && cur.moveToFirst())
			addFromID(cur.getInt(cur.getColumnIndex(DatabaseAdapter.ID))); 
		else
		{
			Intent i = new Intent(this, NewDBItemActivity.class);
			i.putExtra(Constants.ARGS_EAN, EAN);
			startActivityForResult(i,Constants.ACTIVITY_DB_NEW_ITEM); 
		}
	}
	
	
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
		
		if(datepickerBug == 1){
			Calendar c = Calendar.getInstance(); 
			fridge.put(DatabaseAdapter.FRIDGE_PRODUCT_ADDED,c.getTimeInMillis()); 
			c.set(Calendar.YEAR, year);
			c.set(Calendar.MONTH, monthOfYear);
			c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
			fridge.put(DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE, c.getTimeInMillis());
			
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			if(sp.getBoolean("warning", false)){
				fridge.put(DatabaseAdapter.FRIDGE_NUM_DAYS_WARNING, sp.getInt("warning_num", 0));
				commit(); 
			}
			else
				chooseDays(); 

		}
		datepickerBug++; 
	}
	
	private void commit() {
		getContentResolver().insert(DatabaseAdapter.FRIDGE_URI,fridge); 
		finish(); 
	}

	private void chooseDays(){
		@SuppressLint("InflateParams") // http://www.doubleencore.com/2013/05/layout-inflation-as-intended/ (read part about "Every Rule Has An Exception)
		View v = getLayoutInflater().inflate(R.layout.dialog_numberpicker,null);
		final NumberPicker np = (NumberPicker) v.findViewById(R.id.numberPicker1); 
		np.setMinValue(Constants.NUM_PICK_MIN);
		np.setMaxValue(Constants.NUM_PICK_MAX);
		np.setWrapSelectorWheel(false); 
		
		AlertDialog alert = new AlertDialog.Builder(this)
		.setTitle(R.string.reminder)
		.setMessage(R.string.reminder_message)
		.setView(v)
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				fridge.put(DatabaseAdapter.FRIDGE_NUM_DAYS_WARNING,np.getValue());
				commit(); 
			}
		}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which){
				finish();
			}
		}).create();
		 alert.setCanceledOnTouchOutside(false);
		 alert.show(); 
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if( requestCode == Constants.ACTIVITY_DB_NEW_ITEM){
			if( resultCode == Activity.RESULT_OK){
				addFromID(data.getIntExtra(Constants.ARGS_ID, 0));
			}
		}
	}
} // End of class CheckInActivity
