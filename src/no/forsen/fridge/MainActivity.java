package no.forsen.fridge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.abhi.barcode.frag.libv2.IScanResultHandler;
import com.abhi.barcode.frag.libv2.ScanResult;

public class MainActivity extends Activity implements IScanResultHandler{

	private Button btn_in,btn_out,btn_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_in = ((Button)findViewById(R.id.scan_in));
        btn_in.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
		        ScanFragment sf = new ScanFragment(); 
		        sf.show(getFragmentManager(), Constants.FRAGMENT_SCAN_IN);
			}
        	
        });
        btn_out = ((Button)findViewById(R.id.scan_out));
        btn_out.setOnClickListener(new OnClickListener(){
        	@Override
        	public void onClick(View v){
        		ScanFragment sf = new ScanFragment();
        		sf.show(getFragmentManager(),Constants.FRAGMENT_SCAN_OUT);
        	}
        });
        btn_list = ((Button)findViewById(R.id.btn_liste));
        btn_list.setOnClickListener(new OnClickListener(){
        	@Override
        	public void onClick(View v){
        		Intent i = new Intent(v.getContext(), FridgeContentsActivity.class);
        		startActivityForResult(i,Constants.ACTIVITY_FRIDGE_CONTENTS); 
        	}
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
        	Intent i = new Intent(this, SettingsActivity.class);
        	startActivityForResult(i,Constants.ACTIVITY_SETTINGS);
        }
        return super.onOptionsItemSelected(item);
    }


	@Override
	public void scanResult(ScanResult result) {
		Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE); 
		if(v.hasVibrator())
			v.vibrate(200); 
		
		ScanFragment sf_in = (ScanFragment)getFragmentManager().findFragmentByTag(Constants.FRAGMENT_SCAN_IN);
		ScanFragment sf_out = (ScanFragment)getFragmentManager().findFragmentByTag(Constants.FRAGMENT_SCAN_OUT);
		Intent i = null; 
		if(sf_in != null && sf_in.isVisible()){
			i = new Intent(this,CheckInActivity.class);
			sf_in.dismiss();
		}
		else if(sf_out != null && sf_out.isVisible()){			
			i = new Intent(this, CheckOutActivity.class);
			sf_out.dismiss(); 
		}
		else
		{
			try{
				sf_out.dismiss();
				sf_in.dismiss();
			}
			catch(Exception e)
			{
				// do nothing
			}
		}
		i.putExtra(Constants.ARGS_EAN, Long.parseLong(result.getRawResult().toString())); 
		startActivity(i); 
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if(requestCode == Constants.ACTIVITY_SETTINGS){
			Intent i = new Intent(this, PeriodicService.class);
			startService(i); 
		}	
	}
} // End of class MainActivity
