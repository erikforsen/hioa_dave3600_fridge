package no.forsen.fridge;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver{
	@Override
	public void onReceive(Context context, Intent intent){
		Intent i = new Intent(context, PeriodicService.class);
		context.startService(i); 
	}
} // End of class BootReceiver