package no.forsen.fridge;

import java.text.DateFormat;

import android.app.DialogFragment;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItemDialogFragment extends DialogFragment{
	Cursor cur;
	Cursor other; 
	DateFormat df; 
	public static ItemDialogFragment newInstance(int id){
		ItemDialogFragment idf = new ItemDialogFragment();
		Bundle args = new Bundle();
		args.putInt(Constants.ARGS_ID, id);
		idf.setArguments(args);
		return idf; 
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		int id = getArguments().getInt(Constants.ARGS_ID); 
		Uri uri = ContentUris.withAppendedId(DatabaseAdapter.FRIDGE_URI,id );
		String[] queryArgs = {String.valueOf(id)};
		String selection = DatabaseAdapter.DB_TBL_FRIDGE + "." +DatabaseAdapter.ID + "=?";
		cur = getActivity().getContentResolver().query(uri, null, selection, queryArgs, null);
		if(cur!= null && cur.moveToFirst())
		{
			String[] queryArgs2 = {cur.getString(cur.getColumnIndex(DatabaseAdapter.ID))};
			selection = DatabaseAdapter.FRIDGE_PRODUCT_REMOVED +" is null AND ";
			selection += DatabaseAdapter.DB_TBL_DB + "."+DatabaseAdapter.ID+"=?";
			String orderBy = DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE; 
			other = getActivity().getContentResolver().query(DatabaseAdapter.FRIDGE_URI, null, selection, queryArgs2, orderBy);
		}
		df = android.text.format.DateFormat.getDateFormat(getActivity().getApplicationContext());

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		getDialog().setTitle(cur.getString(cur.getColumnIndex(DatabaseAdapter.DB_PRODUCT_NAME)));
		View v = inflater.inflate(R.layout.dialog_item, container); 
		TextView added = (TextView)v.findViewById(R.id.dialog_item_added);
		TextView expired = (TextView)v.findViewById(R.id.dialog_item_expired); 
		TextView others = (TextView)v.findViewById(R.id.dialog_item_other);
		TextView expire_first = (TextView)v.findViewById(R.id.dialog_item_other_expired_first);
		TextView expire_last = (TextView)v.findViewById(R.id.dialog_item_other_expired_last);
		added.setText(df.format(cur.getLong(cur.getColumnIndex(DatabaseAdapter.FRIDGE_PRODUCT_ADDED))));
		expired.setText(df.format(cur.getLong(cur.getColumnIndex(DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE))));
		others.setText(String.valueOf(other.getCount()));
		if(other.getCount()==1){
			v.findViewById(R.id.layout_expire_first).setVisibility(View.GONE);
			v.findViewById(R.id.layout_expire_last).setVisibility(View.GONE); 
			
		}
		else{
			other.moveToFirst();
			expire_first.setText(df.format(other.getLong(other.getColumnIndex(DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE))));
			other.moveToLast();
			expire_last.setText(df.format(other.getLong(other.getColumnIndex(DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE))));
		}
		return v; 
	}
} // End of class ItemDialogFragment
