package no.forsen.fridge;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abhi.barcode.frag.libv2.BarcodeFragment;
import com.abhi.barcode.frag.libv2.IScanResultHandler;

public class ScanFragment extends DialogFragment{
	
	static ScanFragment newInstance(){
		ScanFragment f = new ScanFragment();
		return f; 
	}
	

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceStatu){
		
		getDialog().setTitle(R.string.barcode);
		BarcodeFragment bf = new BarcodeFragment(); 
		
		View v = inflater.inflate(R.layout.dialog_scan, container, false);
		IScanResultHandler p = (IScanResultHandler)getActivity(); 

		bf.setScanResultHandler(p);
		getChildFragmentManager().beginTransaction().add(R.id.fragment_container, bf,Constants.FRAGMENT_SCAN).commit(); 
		return v;
	}
} // End of class ScanFragment
