package no.forsen.fridge;

import java.util.Calendar;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.ViewBinder;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class FridgeContentsFragment extends ListFragment implements LoaderCallbacks<Cursor>,OnNavigationListener {

	LoaderManager loaderManager; 
	CursorLoader cursorLoader;
	SimpleCursorAdapter mAdapter; 
	static int selected = 0; 
	String sortOrder = null;
	private String currentQuery = null; 
	
	final private OnQueryTextListener queryListener = new OnQueryTextListener(){
		@Override
		public boolean onQueryTextChange(String newText){
			if(TextUtils.isEmpty(newText))
				currentQuery = null;
			else 
				currentQuery = newText;
			
			getLoaderManager().restartLoader(0,null,FridgeContentsFragment.this);
			return false; 
		}
		
		@Override
		public boolean onQueryTextSubmit(String query){
			return false; 
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(R.layout.list_content, container,false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		TextView header = new TextView(getActivity());
		header.setText(R.string.fridge);
		header.setGravity(Gravity.CENTER);
		header.setPadding(0, 10, 0, 10);
		header.setBackgroundColor(0xFFDCE4EB);
		header.setTextAppearance(getActivity(),  android.R.style.TextAppearance_Large);
        header.setLayoutParams(new AbsListView.LayoutParams(
        AbsListView.LayoutParams.MATCH_PARENT,
        AbsListView.LayoutParams.WRAP_CONTENT));
		getListView().addHeaderView(header);
		SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
		        R.array.fridge_sortorder, android.R.layout.simple_spinner_dropdown_item);
		ActionBar ab = getActivity().getActionBar();
		ab.setDisplayShowTitleEnabled(false);
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		ab.setListNavigationCallbacks(mSpinnerAdapter, this);
		loaderManager = getActivity().getLoaderManager(); 
		String[] uiBindFrom = {DatabaseAdapter.DB_PRODUCT_NAME, DatabaseAdapter.FRIDGE_NUM_DAYS_WARNING}; 
		int[] uiBindTo = {android.R.id.text1, android.R.id.text2};
		mAdapter = new SimpleCursorAdapter(getActivity().getBaseContext(), R.layout.custom_list_item, null, uiBindFrom, uiBindTo, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		mAdapter.setViewBinder(new ViewBinder(){

			@Override
			public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
				if( columnIndex != cursor.getColumnIndex(DatabaseAdapter.DB_PRODUCT_NAME))
				{
					Calendar eday = toCalendar(Long.parseLong(cursor.getString(cursor.getColumnIndex(DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE))));
					Calendar today = toCalendar(System.currentTimeMillis());
					long diff = eday.getTimeInMillis() - today.getTimeInMillis(); 
					int daysLeft = (int)(diff/(DateUtils.DAY_IN_MILLIS)); 
					int color = Color.BLACK; 
					if(daysLeft < Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseAdapter.FRIDGE_NUM_DAYS_WARNING))))
						color = Color.YELLOW;
					if(daysLeft < 1)
						color = Color.RED; 
					

					if( columnIndex == cursor.getColumnIndex(DatabaseAdapter.FRIDGE_NUM_DAYS_WARNING))
					{

						TextView tv = (TextView) view; 
						String message = Math.abs(daysLeft)==1?getResources().getString(R.string.day):getResources().getString(R.string.days); 
						
						tv.setTextColor(color); 
						
						if(daysLeft < 1)
							message = getResources().getString(R.string.expired)+ " " + Math.abs(daysLeft) + " " + message +" " + getResources().getString(R.string.ago);
						else
							message = daysLeft + " " + message +" "+ getResources().getString(R.string.left); 
						tv.setText(message);
					}
					return true;
				}
				return false; 
			}
			
		});
		setHasOptionsMenu(true); 
		setListAdapter(mAdapter); 
		
		final ListView lv = getListView();
		lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		lv.setMultiChoiceModeListener(new MultiChoiceModeListener(){

			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				MenuInflater inflater = mode.getMenuInflater();
				inflater.inflate(R.menu.context_fridge, menu);
				return true;
			}

			@Override
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
				return false;
			}

			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
				switch(item.getItemId()){
				case R.id.action_delete:
					ContentValues cv = new ContentValues();
					SparseBooleanArray sparseBooleanArray = lv.getCheckedItemPositions(); 
					for( int i = 0; i<lv.getCount(); i++){	
						if(sparseBooleanArray.get(i)){
							cv.put(DatabaseAdapter.FRIDGE_PRODUCT_REMOVED, Calendar.getInstance().getTimeInMillis()); 
							String[] updateArgs = {String.valueOf(((Cursor)lv.getItemAtPosition(i)).getString(0))}; 
							getActivity().getContentResolver().update(DatabaseAdapter.FRIDGE_URI, cv, DatabaseAdapter.ID +"= ?", updateArgs);
						}
					}
				}
				selected = 0;
				mode.finish();
				return true;
			}

			@Override
			public void onDestroyActionMode(ActionMode mode) {
				selected = 0; 
			}

			@Override
			public void onItemCheckedStateChanged(ActionMode mode,
					int position, long id, boolean checked) {
				if(checked)
					selected++;
				else
					selected--; 
				
				mode.setTitle(selected +" "+ getResources().getString(R.string.selected));
			}
			
		}); 
		
		
		loaderManager.initLoader(Constants.SORT_ORDER_EXPIRE_ASC, null, this); 
	}
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		 
		if(id == Constants.SORT_ORDER_NAME_ASC)
			sortOrder = DatabaseAdapter.DB_PRODUCT_NAME + " ASC";
		else if(id == Constants.SORT_ORDER_EXPIRE_ASC)
			sortOrder = DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE + " ASC"; 
	
		String selection = DatabaseAdapter.FRIDGE_PRODUCT_REMOVED + " is null";
		String[] selectionArgs = null; 
		if(!TextUtils.isEmpty(currentQuery)){
			selection += " AND " + DatabaseAdapter.DB_PRODUCT_NAME +" LIKE ?";
			selectionArgs = new String[] {"%"+currentQuery+"%"};
		}
		return new CursorLoader(getActivity().getBaseContext(), DatabaseAdapter.FRIDGE_URI, null, selection, selectionArgs, sortOrder);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		if(mAdapter != null && data != null)
			mAdapter.swapCursor(data); 
		
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		if(mAdapter != null)
			mAdapter.swapCursor(null);		
	}
	
	private Calendar toCalendar(long timeinmillis){
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timeinmillis);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c; 
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		
		if(itemPosition == 1)
			getLoaderManager().restartLoader(Constants.SORT_ORDER_NAME_ASC, null, this);  
		else
			getLoaderManager().restartLoader(Constants.SORT_ORDER_EXPIRE_ASC, null, this); 
		
		
		return false;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
		inflater.inflate(R.menu.fridge_menu, menu);
		SearchView searchView = (SearchView)menu.findItem(R.id.fridge_search).getActionView();
		searchView.setOnQueryTextListener(queryListener); 
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()){
		case R.id.db_contents:
			Intent i = new Intent(getActivity(), DBContentsActivity.class);
			getActivity().startActivityForResult(i, Constants.ACTIVITY_DB_CONTENTS);
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
		Cursor cursor = (Cursor) getListAdapter().getItem(position-1);
		int curId = cursor.getInt(0);

		ItemDialogFragment details = ItemDialogFragment.newInstance(curId);
        details.show(getChildFragmentManager(), Constants.FRAGMENT_ITEM);
	}
} // End of class FridgeContentsFragment
