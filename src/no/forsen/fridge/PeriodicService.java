package no.forsen.fridge;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class PeriodicService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		if( !settings.getBoolean("enabled", true)) 
			return super.onStartCommand(intent, flags, startId); 
		
		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		
		Intent i = new Intent(this,NotifyService.class);
		PendingIntent pi = PendingIntent.getService(this,0,i,0);
		Calendar c = Calendar.getInstance(); 
		c.set(Calendar.HOUR_OF_DAY, 1); 
		alarm.setRepeating(AlarmManager.RTC, c.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pi);
		return super.onStartCommand(intent, flags, startId);
	}
} // End of class PeriodicService
