package no.forsen.fridge;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseAdapter extends ContentProvider{
	static final String DB_NAME="fridge.db";
	static final String DB_TBL_FRIDGE="fridge";
	static final String DB_TBL_DB = "db";
	static final String ID = BaseColumns._ID;
	static final String DB_PRODUCT_NAME="product_name";
	static final String DB_EAN = "ean";
	static final String DB_FREQUENCY = "freq"; 
	static final String FRIDGE_PRODUCT_ADDED="added";
	static final String FRIDGE_PRODUCT_REMOVED="removed";
	static final String FRIDGE_PRODUCT_EXPIRE="expire";
	static final String FRIDGE_PRODUCT = "product"; 
	static final String FRIDGE_NUM_DAYS_WARNING="warning";
	static final int DB_VERSION=1;
	
	private final static int DB = 1;
	private final static int MDB =2;
	private final static int FRIDGE = 3;
	private final static int MFRIDGE = 4; 
	
	static final String PROVIDER="no.forsen.fridge";
	
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db; 
	
	static final Uri DB_URI = Uri.parse("content://" + PROVIDER + "/" + DB_TBL_DB); 
	static final Uri FRIDGE_URI = Uri.parse("content://"+PROVIDER+"/"+DB_TBL_FRIDGE); 
	
	private static final UriMatcher uriMatcher; 
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER, DB_TBL_DB, MDB);
		uriMatcher.addURI(PROVIDER, DB_TBL_DB+"/#", DB);
		uriMatcher.addURI(PROVIDER, DB_TBL_FRIDGE, MFRIDGE);
		uriMatcher.addURI(PROVIDER, DB_TBL_FRIDGE+"/#", FRIDGE);
	}
	@Override
	public boolean onCreate() {
		DBHelper = new DatabaseHelper(getContext()); 
		db=DBHelper.getReadableDatabase();
		return true;
	}
	
	public Uri insert(Uri uri, ContentValues values){
		Cursor cur;
		if(uriMatcher.match(uri) == MDB) {
			db.insert(DB_TBL_DB, null, values);
			cur = db.query(DB_TBL_DB, null, null, null, null, null, null);
		}
		else if(uriMatcher.match(uri) == MFRIDGE){
			db.insert(DB_TBL_FRIDGE, null, values);
			cur = db.query(DB_TBL_FRIDGE, null, null, null, null, null, null);
		}
		else 
			cur = null; 
		cur.moveToLast();
		long myid = cur.getLong(0);
		getContext().getContentResolver().notifyChange(uri,null); 
		return ContentUris.withAppendedId(uri, myid); 
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Cursor cur = null; 
		SQLiteQueryBuilder _QB = new SQLiteQueryBuilder(); 
		_QB.setTables(DB_TBL_FRIDGE + " INNER JOIN " + DB_TBL_DB + " ON " + FRIDGE_PRODUCT + " = " + DB_TBL_DB+"."+ID); 
		switch(uriMatcher.match(uri)){
		case DB: 
			cur=db.query(DB_TBL_DB, projection, selection, selectionArgs, null, null, sortOrder);
			break; 
		case MDB:
			cur=db.rawQuery("SELECT "+DB_TBL_DB+".*, COUNT(product) AS " + DB_FREQUENCY + " FROM " + DB_TBL_DB + " LEFT JOIN "+DB_TBL_FRIDGE +" ON "+DB_TBL_DB+"."+ID + " = "+DB_TBL_FRIDGE + "."+FRIDGE_PRODUCT +" GROUP BY "+DB_TBL_DB+"."+ID+" ORDER BY " + sortOrder +";",null);
			break;
		case FRIDGE:
			cur=_QB.query(db, null, selection, selectionArgs, null, null, sortOrder); 
			break;
		case MFRIDGE:
			cur=_QB.query(db, projection, selection, selectionArgs, null, null, sortOrder);
			break;
		}
		
		cur.setNotificationUri(getContext().getContentResolver(), uri);
		return cur;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		switch(uriMatcher.match(uri)){
			case DB:
				db.delete(DB_TBL_DB, ID + " = " + uri.getPathSegments().get(1), null);
				getContext().getContentResolver().notifyChange(uri, null);
				return DB; 
			case MDB:
				db.delete(DB_TBL_DB, null, null);
				return MDB;
			case FRIDGE:
				db.delete(DB_TBL_FRIDGE, selection, selectionArgs);
				return FRIDGE;
			case MFRIDGE:
				db.delete(DB_TBL_FRIDGE, null,null);
				return MFRIDGE;
		}
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		switch(uriMatcher.match(uri)){
		case MDB:
		case DB:
			db.update(DB_TBL_DB, values, selection,selectionArgs);
			getContext().getContentResolver().notifyChange(uri, null); 
			return DB;
		case MFRIDGE:
		case FRIDGE:
			db.update(DB_TBL_FRIDGE, values, selection, selectionArgs); 
			getContext().getContentResolver().notifyChange(uri, null);
			return FRIDGE; 
		}
		return 0;
	}
	
	
	private static class DatabaseHelper extends SQLiteOpenHelper{
		DatabaseHelper(Context context){
			super(context,DB_NAME,null,DB_VERSION);
		}
		@Override
		public void onOpen(SQLiteDatabase db){
			db.execSQL("PRAGMA foreign_keys = on;");
		}
		@Override
		public void onCreate(SQLiteDatabase db){
			String sql1 = "CREATE TABLE " + DB_TBL_DB + " (" + ID + " integer primary key autoincrement, "
				+ DB_EAN + " integer, " + DB_PRODUCT_NAME + " text);";
			db.execSQL(sql1); 
			
			String sql2 = "CREATE TABLE " + DB_TBL_FRIDGE + " (" + ID + " integer primary key autoincrement, "
				+ FRIDGE_PRODUCT_ADDED + " integer, " + FRIDGE_PRODUCT_REMOVED + " integer, "
				+ FRIDGE_PRODUCT_EXPIRE + " integer, "
				+ FRIDGE_NUM_DAYS_WARNING + " integer, "
				+ FRIDGE_PRODUCT + " integer, "
				+ "FOREIGN KEY (" + FRIDGE_PRODUCT + ") REFERENCES " + DB_TBL_DB + " ("+ID+") ON DELETE CASCADE);";
			db.execSQL(sql2); 
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
			db.execSQL("DROP TABLE IF EXISTS " + DB_TBL_DB);
			db.execSQL("DROP TABLE IF EXISTS " + DB_TBL_FRIDGE);
			onCreate(db); 
		}
	
	}// End of inner class DatabaseHelper

} // End of class DatabaseAdapter
