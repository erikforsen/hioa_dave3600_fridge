package no.forsen.fridge;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener{
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.preferences);
	}

	@Override
	public void onResume(){
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this); 
		initSummary(); 
	}
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		updatePrefsSummary(sharedPreferences, findPreference(key));		
	}
	
	public void onPause(){
		super.onPause(); 
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this); 
	}
	
	protected void updatePrefsSummary(SharedPreferences sp, Preference p){
		if(p == null)
			return; 
		
	
		if(p instanceof NumberPreference){
			NumberPreference np = (NumberPreference) p; 
			np.setSummary(np.getValue()); 
		}
	}
	
	protected void initSummary(){
		for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++){
			updatePrefsSummary(getPreferenceManager().getSharedPreferences(), getPreferenceScreen().getPreference(i)); 
		}
	}
} // End of class SettingsFragment