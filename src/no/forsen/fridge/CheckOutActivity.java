package no.forsen.fridge;

import java.util.Calendar;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.Toast;

public class CheckOutActivity extends Activity {
	private long ean;
	@Override
	protected void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Intent i = getIntent(); 
		Bundle args = i.getExtras();
		ean = (Long) args.get(Constants.ARGS_EAN); 
		Uri uri = ContentUris.withAppendedId(DatabaseAdapter.FRIDGE_URI, ean);
		String[] queryArgs = {String.valueOf(ean)};
		String selection = DatabaseAdapter.DB_TBL_DB+"."+DatabaseAdapter.DB_EAN+"=? AND " + DatabaseAdapter.FRIDGE_PRODUCT_REMOVED + " is null";
		Cursor cur = getContentResolver().query(uri, null, selection, queryArgs, DatabaseAdapter.FRIDGE_PRODUCT_EXPIRE + " DESC"); 
		if(cur != null && cur.moveToFirst())
		{
			ContentValues cv = new ContentValues(); 
			cv.put(DatabaseAdapter.FRIDGE_PRODUCT_REMOVED, Calendar.getInstance().getTimeInMillis()); 
			String[] updateArgs = {cur.getString(0)}; 
			getContentResolver().update(uri, cv, DatabaseAdapter.ID+" = ?", updateArgs);
		}
		else{
			Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE); 
			if(v.hasVibrator())
				v.vibrate(200); 
			Toast.makeText(this, R.string.notfound, Toast.LENGTH_LONG).show(); 
		}
	}
} // End of class CheckOutActivity
