package no.forsen.fridge;

final class Constants {
	final static int ACTIVITY_DB_CONTENTS = 0; 
	final static int ACTIVITY_FRIDGE_CONTENTS = 1; 
	final static int ACTIVITY_CHECK_IN = 2; 
	final static int ACTIVITY_CHECK_OUT = 3; 
	final static int ACTIVITY_DB_NEW_ITEM = 7; 
	final static int ACTIVITY_SETTINGS = 8; 
	final static int SORT_ORDER_NAME_ASC = 4;
	final static int SORT_ORDER_FREQUENCY_DESC = 5;
	final static int SORT_ORDER_EXPIRE_ASC = 6;
	final static String ARGS_EAN = "ean"; 
	final static String ARGS_ID = "id"; 
	final static String FRAGMENT_DATEPICKER = "frag_date"; 
	final static String FRAGMENT_SCAN_IN = "frag_scan_in";
	final static String FRAGMENT_SCAN_OUT = "frag_scan_out";
	final static String FRAGMENT_SCAN = "scan";
	final static String FRAGMENT_ITEM = "frag_item";
	
	final static int NUM_PICK_MIN = 0; 
	final static int NUM_PICK_MAX = 100; 
	private Constants(){
		
	}

}  // End of class Constants
