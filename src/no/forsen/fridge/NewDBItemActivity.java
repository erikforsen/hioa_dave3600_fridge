package no.forsen.fridge;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewDBItemActivity extends Activity {
	private EditText input; 
	private long EAN; 
	private boolean valid; 
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		final Intent i = getIntent(); 
		EAN = i.getLongExtra(Constants.ARGS_EAN, 0L);
		input = new EditText(this);
		input.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s)
			{
				valid = Validator.textInput(input, true, getResources().getString(R.string.unvalidated), getResources().getString(R.string.required));
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
		});
		AlertDialog builder = new AlertDialog.Builder(this)
			.setTitle(R.string.new_product)
			.setMessage(R.string.new_product_message)
			.setView(input)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which){
					setResult(RESULT_CANCELED);
					finish();
				}
			}).create();
		builder.setCanceledOnTouchOutside(false);
		builder.show();
		Button okButton = builder.getButton(DialogInterface.BUTTON_POSITIVE);
		okButton.setOnClickListener(new CustomListener(builder));
		
		
		
	}
	
	class CustomListener implements View.OnClickListener {
		private Dialog dialog;
		public CustomListener(Dialog dialog){
			this.dialog = dialog; 
		}
		
		@Override
		public void onClick(View v) {
			if(valid){
				ContentValues cv = new ContentValues();
				if(EAN != 0L)
					cv.put(DatabaseAdapter.DB_EAN, EAN); 
				cv.put(DatabaseAdapter.DB_PRODUCT_NAME, input.getText().toString());
				Uri uri = getContentResolver().insert(DatabaseAdapter.DB_URI, cv);
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				Intent i = getIntent(); 
				i.putExtra(Constants.ARGS_ID, id);
				setResult(RESULT_OK,i); 
				dialog.dismiss(); 
				finish(); 
			}
			else if(input.getError() == null)
				Validator.textInput(input, true, getResources().getString(R.string.unvalidated), getResources().getString(R.string.required));

		}
	} // End of inner class CustomListener 
} // End of class NewDBItemActivity
