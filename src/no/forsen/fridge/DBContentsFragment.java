package no.forsen.fridge;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.SimpleCursorAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class DBContentsFragment extends ListFragment implements LoaderCallbacks<Cursor>,OnNavigationListener{

	LoaderManager loaderManager;
	CursorLoader cursorLoader; 
	SimpleCursorAdapter mAdapter;
	static int selected = 0; 
	String sortOrder = null;
	private String currentQuery = null;
	private boolean valid;
	private EditText name;
	private EditText ean; 
	private long updateId;
	
	final private OnQueryTextListener queryListener = new OnQueryTextListener(){
		@Override
		public boolean onQueryTextChange(String newText){
			if(TextUtils.isEmpty(newText))
				currentQuery = null;
			else
				currentQuery = newText;
			
			getLoaderManager().restartLoader(0,null,DBContentsFragment.this);
			return false; 
		}
		@Override
		public boolean onQueryTextSubmit(String query){
			return false; 
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(android.R.layout.list_content,  container, false); 
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		TextView header = new TextView(getActivity());
		header.setText(R.string.productdb);
		header.setGravity(Gravity.CENTER);
		header.setPadding(0, 10, 0, 10);
		header.setBackgroundColor(0xFFDCE4EB);
		header.setTextAppearance(getActivity(),  android.R.style.TextAppearance_Large);
        header.setLayoutParams(new AbsListView.LayoutParams(
        AbsListView.LayoutParams.MATCH_PARENT,
        AbsListView.LayoutParams.WRAP_CONTENT));
		getListView().addHeaderView(header);
		SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
		        R.array.db_sortorder, android.R.layout.simple_spinner_dropdown_item);
		ActionBar ab = getActivity().getActionBar();
		ab.setDisplayShowTitleEnabled(false);
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		ab.setListNavigationCallbacks(mSpinnerAdapter, this);
		loaderManager = getActivity().getLoaderManager();
		String[] uiBindFrom = {DatabaseAdapter.DB_PRODUCT_NAME, DatabaseAdapter.DB_EAN};
		int[] uiBindTo = {android.R.id.text1, android.R.id.text2};
		mAdapter = new SimpleCursorAdapter(getActivity().getBaseContext(), android.R.layout.simple_list_item_activated_2, null, uiBindFrom, uiBindTo,SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		
		setListAdapter(mAdapter);
		setHasOptionsMenu(true);
		final ListView lv = getListView(); 
		lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		lv.setMultiChoiceModeListener(new MultiChoiceModeListener(){

			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				MenuInflater inflater = mode.getMenuInflater(); 
				inflater.inflate(R.menu.context_db, menu);
				return true;
			}

			@Override
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
				MenuItem item = menu.findItem(R.id.action_edit);
				item.setVisible(selected==1);
				return true; 
			}

			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
				long[] ids = lv.getCheckedItemIds();
				switch(item.getItemId()){
				case R.id.action_delete: 
					getConfirmation(ids); 
					break; 
				case R.id.action_edit:
					editDBItem(ids[0]); 
					break; 
				}
				mode.finish(); 
				return true;
			}

			@Override
			public void onDestroyActionMode(ActionMode mode) {
				selected = 0; 				
			}

			@Override
			public void onItemCheckedStateChanged(ActionMode mode,
					int position, long id, boolean checked) {
				if(checked)
					selected++;
				else
					selected--;
				mode.setTitle(selected + " " + getResources().getString(R.string.selected));
				mode.invalidate(); 
			}
			
		});
		loaderManager.initLoader(Constants.SORT_ORDER_NAME_ASC, null, this);
		
	}
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		
		if(id == Constants.SORT_ORDER_FREQUENCY_DESC)
			sortOrder = DatabaseAdapter.DB_FREQUENCY + " DESC"; 
		else if(id == Constants.SORT_ORDER_NAME_ASC)
			sortOrder = DatabaseAdapter.DB_PRODUCT_NAME + " ASC";
		
		String selection = null; 
		String[] selectionArgs = null;
		if(!TextUtils.isEmpty(currentQuery)){
			selection = DatabaseAdapter.DB_PRODUCT_NAME + " LIKE ?";
			selectionArgs = new String[] {"%"+currentQuery+"%"};
		}
			
		return new CursorLoader(getActivity().getBaseContext(), DatabaseAdapter.DB_URI,null,selection,selectionArgs,sortOrder);
	}
	
	@Override 
	public void onListItemClick(ListView l, View v, int position, long id){
		Cursor cursor = (Cursor) getListAdapter().getItem(position);
		Intent i = getActivity().getIntent();
		i.putExtra(Constants.ARGS_ID, Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseAdapter.ID))));
		getActivity().setResult(Activity.RESULT_OK,i); 
		getActivity().finish(); 
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		if(mAdapter != null && data != null)
			mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		if(mAdapter != null)
			mAdapter.swapCursor(null); 
	}
	
	private void getConfirmation(long[] id)
	{
		final long[] ids = id;
		String message = getResources().getString(R.string.all_occurrences);
		message += ids.length > 1?getResources().getString(R.string.these_products):getResources().getString(R.string.this_product);
		message += getResources().getString(R.string.will_be_removed);
		AlertDialog alert = new AlertDialog.Builder(getActivity())
			.setTitle(android.R.string.dialog_alert_title)
			.setIconAttribute(android.R.attr.alertDialogIcon)
			.setMessage(message)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					for(int i = 0; i < ids.length; i++)
					{
						Uri uri = ContentUris.withAppendedId(DatabaseAdapter.DB_URI, ids[i]);
						getActivity().getContentResolver().delete(uri, null, null); 
					}
					
				}
			}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which){
				}
			}).create();
		alert.setCanceledOnTouchOutside(false);
		alert.show();
	}
	private void editDBItem(long id){
		updateId = id; 
		Uri uri = ContentUris.withAppendedId(DatabaseAdapter.DB_URI, id);
		String selection = DatabaseAdapter.ID + "=?";
		String[] selectionArgs = {String.valueOf(id)};
		Cursor cur = getActivity().getContentResolver().query(uri, null, selection, selectionArgs, null);
		@SuppressLint("InflateParams") 
		View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_edit_db_item, null);
		name = (EditText) v.findViewById(R.id.product_name);
		ean = (EditText) v.findViewById(R.id.product_ean); 
		
		if(cur != null && cur.moveToFirst())
		{
			name.setText(cur.getString(cur.getColumnIndex(DatabaseAdapter.DB_PRODUCT_NAME)));
			ean.setText(cur.getString(cur.getColumnIndex(DatabaseAdapter.DB_EAN)));
		}
		name.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s){
				valid = Validator.textInput(name, true, getResources().getString(R.string.unvalidated), getResources().getString(R.string.required));
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
		});
		ean.addTextChangedListener(new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				valid = Validator.numberInput(ean, false, getResources().getString(R.string.unvalidated), getResources().getString(R.string.required));				
			}
			
		});
		AlertDialog builder = new AlertDialog.Builder(getActivity())
		.setTitle(R.string.edit_item)
		.setMessage(R.string.update_fields)
		.setView(v)
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
	
			}
		}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which){

			}
		}).create();
	builder.setCanceledOnTouchOutside(false);
	builder.show();
	Button okButton = builder.getButton(DialogInterface.BUTTON_POSITIVE);
	okButton.setOnClickListener(new CustomListener(builder)); 
	}
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if(itemPosition == 1)
			getLoaderManager().restartLoader(Constants.SORT_ORDER_FREQUENCY_DESC,null,this);
		else
			getLoaderManager().restartLoader(Constants.SORT_ORDER_EXPIRE_ASC, null, this); 
		return false;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
		inflater.inflate(R.menu.db_menu, menu);
		SearchView searchView = (SearchView)menu.findItem(R.id.db_search).getActionView(); 
		searchView.setOnQueryTextListener(queryListener);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		case R.id.new_db_item:
			Intent i = new Intent(getActivity(),NewDBItemActivity.class); 
			startActivity(i); 
		}
		return super.onOptionsItemSelected(item);
	}
	
	class CustomListener implements View.OnClickListener{
		private Dialog dialog;
		public CustomListener(Dialog dialog){
			this.dialog = dialog;
		}
		
		@Override
		public void onClick(View v){
			if(valid){
				ContentValues cv = new ContentValues();
				cv.put(DatabaseAdapter.DB_PRODUCT_NAME, name.getText().toString());
				cv.put(DatabaseAdapter.DB_EAN, ean.getText().toString()); 
				String[] updateArgs = {String.valueOf(updateId)};
				getActivity().getContentResolver().update(DatabaseAdapter.DB_URI, cv, DatabaseAdapter.ID + "=?",updateArgs);
				dialog.dismiss();
			}
			else if(name.getError() == null)
				Validator.textInput(name, true, getResources().getString(R.string.unvalidated), getResources().getString(R.string.required));
			else if(ean.getError() == null)
				Validator.numberInput(ean, false, getResources().getString(R.string.unvalidated), getResources().getString(R.string.required));

		}
	} // End of inner class CustomListener
} // End of class DBContentsFragment