package no.forsen.fridge;

import java.util.regex.Pattern;

import android.widget.EditText;

public class Validator {
	private static final String TEXT_REGEX = "^[a-zA-ZøØæÆåÅ\' 0-9,\\.]*";
	private static final String NUMBER_REGEX = "^[0-9]*"; 
	public static boolean textInput(EditText editText, boolean required, String errMsg, String reqMsg){
		return isValid(editText, TEXT_REGEX, errMsg, reqMsg, required); 
	}
	public static boolean numberInput(EditText editText, boolean required, String errMsg, String reqMsg){
		return isValid(editText, NUMBER_REGEX, errMsg, reqMsg, required); 
	}
	private static boolean isValid(EditText editText, String regex, String errMsg, String reqMsg, boolean required){
		String text = editText.getText().toString().trim(); 
		editText.setError(null);
		
		if(required && !hasText(editText,reqMsg))
			return false; 
		
		if(!Pattern.matches(regex,text)){
			editText.setError(errMsg);
			return false;
		}
		return true; 
	}
	
	private static boolean hasText(EditText editText, String errMsg){
		String text = editText.getText().toString().trim();
		editText.setError(null);
		
		if(text.length() == 0){
			editText.setError(errMsg);
			return false; 
		}
		return true; 
	}
} // End of class Validator
