apputvikling_mappe3
===================

Tredje innlevering i kurset App Utvikling (Android) på Høskolen i Oslo og Akershus

Tredje innlevering er en selvvalgt innlevering. 

Jeg har valgt å lage en app der man skanner strekkoden til varer på vei inn i kjøleskapet (og registrerer utgangsdato), 
og skanner strekkoden når varen / embalasjen er på vei til søpla. På den måten har man til envher tid full oversikt over hvilke
varer man har i kjøleskapet, og holdbarheten til disse varene. Perfekt når man skal planlegge middager, når man står i butikken og 
skal gjøre ukens dagligvarehandel. 

